from src.oop.cars import Car

def test_new_car():
    test_car = Car("Toyota Yaris", 2012)
    assert test_car.total_distance == 0

def test_car_distance_100():
    test_car = Car("Honda Accord", 1996)
    test_car.drive(100)
    assert test_car.total_distance == 100

def test_car_distance_100_and_200():
    test_car = Car("Honda Accord", 1996)
    test_car.drive(100)
    test_car.drive(200)
    assert test_car.total_distance == 300

def test_car_has_warranty():
    test_car = Car("Toyota Avensis", 2020)
    test_car.drive(119000)
    assert test_car.has_warranty() is True

def test_car_has_not_warranty():
    test_car = Car("Toyota Carina", 2007)
    test_car.drive(19000)
    assert test_car.has_warranty() is False

def test_get_the_description():
    test_car = Car("Honda Civic", 1998)
    test_car.drive(230000)
    assert test_car.get_description() == "This is a Honda Civic made in 1998. Currently it drove 230000 kilometers"
