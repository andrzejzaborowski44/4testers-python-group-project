import datetime

# W package “src” tworzymy nowy plik, a w nim klasę Car (samochód). Wymagania co do konstruktora:
# Samochód opisywany jest cechami: model, rocznik, przebieg
# Do stworzenia samochodu wymagane jest podanie modelu i rocznika
# Przebieg każdego nowego samochodu to 0

class Car:
    def __init__(self, model, manufacture_year):
        self.model = model
        self.manufacture_year = manufacture_year
        self.total_distance = total_distance = 0

    def drive(self, total_distance):
        self.total_distance += total_distance

    def has_warranty(self):
        date = datetime.date.today()
        year = date.year
        car_age = year - self.manufacture_year
        if car_age > 7 or self.total_distance > 120000:
            return False
        else:
            return True

#get_description() - zwraca string opisujący auto:
#“This is a MODEL made in MANUFACTURE_YEAR. Currently it drove TOTAL_DISTANCE kilometers”

    def get_description(self):
        return f"This is a {self.model} made in {self.manufacture_year}. Currently it drove {self.total_distance} kilometers"



if __name__ == '__main__':
    car1 = Car("Honda Civic", 2022)
    car2 = Car("Fiat Tipo", 2021)

    car1.drive(70000)
    car2.drive(122000)


    print(car1.has_warranty())
    print(car2.has_warranty())

    print(car1.get_description())
    print(car2.get_description())
